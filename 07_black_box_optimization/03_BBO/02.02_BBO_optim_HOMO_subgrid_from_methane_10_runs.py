#!/usr/bin/env python
# coding: utf-8

# In[1]:


import os

dft_path = "/home/juleguy/dft_comput"

json_cache_location_list = [os.environ["DATA"] + "/00_datasets/DFT/cache_OD9_step7.json",
                            os.environ["DATA"] + "/00_datasets/DFT/cache_OPT_OD9_Marta_filtered.json",
                            os.environ["DATA"] + "/00_datasets/DFT/cache_OPT_OD9_0.json",
                            os.environ["DATA"] + "/00_datasets/DFT/cache_OPT.json"]

init_dataset_smiles = ["C"]

test_dataset_path = os.environ["DATA"] + "/00_datasets/DFT/OD9_7_smi_datasets/validation.smi"

output_data_path = os.environ["DATA"] + "/07_BBO/03_bbo_optim/02.02_optim_HOMO_subgrid_from_methane_10_runs"

# Parallel parameters
n_jobs_per_model = 10

# BBO parameters
n_models_bagging = 10
test_dataset_size = 1000
save_surrogate_model = False
period_save = 1
period_compute_test_predictions = 1
max_obj_calls = 1000

# EvoMol parameters
evomol_pop_max_size = 300
evomol_max_steps = 50
evomol_k_to_replace = 10
evomol_init_pop_size = 10
evomol_n_runs = 10
evomol_n_best_retrieved = 1
evomol_init_pop_strategy = "random_weighted"

# Problem action space
max_heavy_atoms = 9
heavy_atoms = "C,N,O,F"


# In[2]:


from bbo.objective_dft import DFTEnergyObjective
from bbo.objective import QEDObjective

objective = DFTEnergyObjective(property="homo", cache_location=None, json_cache_location_list=json_cache_location_list,
                               n_jobs=n_jobs_per_model, working_dir=dft_path)

# objective = QEDObjective(cache_location=None, n_jobs=n_jobs_per_model)


# In[ ]:





# In[3]:


from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.gaussian_process.kernels import RBF, Matern, DotProduct
from sklearn.kernel_ridge import KernelRidge


models = [
    ("GPR(1*RBF())", GaussianProcessRegressor(1 * RBF(1.0), alpha=0.1)),
]


# In[4]:


from bbo.model import BaggingSurrogateModel, GPRSurrogateModelWrapper, DummySurrogateModelWrapper

# tuples (wrapper str name, wrapper class, lambda condition of use with a model m, parameters dict)

surrogate_wrappers = [
    ("GPRWrapper", GPRSurrogateModelWrapper, lambda m : isinstance(m, GaussianProcessRegressor), {}),
    ("Bagging", BaggingSurrogateModel, lambda m: True, {"n_models": n_models_bagging, "n_jobs": n_jobs_per_model}),
    ("Surrogate", DummySurrogateModelWrapper, lambda m: True, {})
]

surrogate_models = []
    
for model in models:
    
    model_name, model_instance = model
    
    for surrogate_wrapper in surrogate_wrappers:

        wrapper_name, wrapper_class, lambda_f, param_dict = surrogate_wrapper
        
        if lambda_f(model_instance):            
            surrogate_models.append(
                (
                    wrapper_name + "(" + model_name + ")", # Surrogate name
                    wrapper_class(base_model=model_instance, **param_dict)
                )
    )
    
print(surrogate_models)


# In[5]:


from bbo.descriptor import SOAPDesc, MBTRDesc, ShinglesVectDesc

descriptors = [
    ("Shingles_count", ShinglesVectDesc(cache_location=None, count=True))
]


# In[6]:


from bbo.merit import ExpectedImprovementMerit, SurrogateValueMerit

# Tuples (merit str name, merit str class, lambda surrogate s : can be used with)

merit_functions = [("Expected_improvement", ExpectedImprovementMerit, lambda s: not isinstance(s, DummySurrogateModelWrapper)),
                   ("Surrogate_value", SurrogateValueMerit, lambda s: isinstance(s, DummySurrogateModelWrapper))]


# In[7]:


def build_grid(surrogate_models, descriptors, merit_functions):
    """
    Returning a list of tuple describing all compatible modules
    """
    
    grid = []
    
    for surrogate_model in surrogate_models:
        for descriptor in descriptors:
            for merit_function in merit_functions:
                    
                if merit_function[2](surrogate_model[1]):
                    grid.append((surrogate_model, descriptor, merit_function))
                        
    return grid
    


# In[9]:


def load_smiles_from_file(path, n_smiles):
    
    smi_list = []
    
    with open(path, "r") as f:
        for line in f.readlines()[:n_smiles]:
            smi_list.append(line.rstrip())
            
    return smi_list
    


# In[10]:


from os.path import join
from bbo.bboalg import BBOAlg
from bbo.stop_criterion import KObjFunCallsFunctionStopCriterion


def run_optimization_model(surrogate_model, descriptor, merit, run_id):

    surrogate_name = surrogate_model[0]
    surrogate_instance = surrogate_model[1]
    descriptor_name = descriptor[0]
    descriptor_instance = descriptor[1]
    merit_name = merit[0]
    Merit_class = merit[1]
    
    model_name = "-".join([surrogate_name, descriptor_name, merit_name])
    model_path = join(join(output_data_path, model_name), str(run_id))
    print(model_name)
    
    test_dataset_smiles = load_smiles_from_file(test_dataset_path, test_dataset_size)
    
    alg = BBOAlg(
        init_dataset_smiles=init_dataset_smiles,
        init_test_dataset_smiles=test_dataset_smiles,
        descriptor=descriptor_instance,
        objective=objective,
        merit_function=Merit_class(descriptor=descriptor_instance, 
                                   pipeline=None, 
                                   surrogate=surrogate_instance),
        surrogate=surrogate_instance,
        pipeline=None,
        stop_criterion=KObjFunCallsFunctionStopCriterion(max_obj_calls),
        evomol_parameters={
            "optimization_parameters": {
                "pop_max_size": evomol_pop_max_size,
                "max_steps": evomol_max_steps,
                "k_to_replace": evomol_k_to_replace
            },
            "action_space_parameters":{
                "max_heavy_atoms": max_heavy_atoms,
                "atoms": heavy_atoms
            }
        },
        evomol_init_pop_size=evomol_init_pop_size,
        n_evomol_runs=evomol_n_runs,
        n_best_evomol_retrieved=evomol_n_best_retrieved,
        evomol_init_pop_strategy=evomol_init_pop_strategy,
        results_path=model_path,
        n_jobs=n_jobs_per_model,
        save_surrogate_model=save_surrogate_model,
        period_save=period_save,
        period_compute_test_predictions=period_compute_test_predictions
        
    )
    
    alg.run()
    


# In[19]:


import sys

if __name__ == "__main__":

    grid = build_grid(surrogate_models, descriptors, merit_functions)
    print(len(grid))
    
    input_param = int(sys.argv[1])
    
    model_id = input_param % len(grid)
    run_id = input_param // len(grid)
    
    surrogate_model, descriptor, merit = grid[model_id]
    
    run_optimization_model(surrogate_model, descriptor, merit, run_id=run_id)


# In[ ]:





# In[ ]:





# In[ ]:




