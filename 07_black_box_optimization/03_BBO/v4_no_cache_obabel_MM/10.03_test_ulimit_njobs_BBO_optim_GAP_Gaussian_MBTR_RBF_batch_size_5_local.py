#!/usr/bin/env python
# coding: utf-8

# In[1]:


import os

dft_path = os.environ["DFT_COMPUT_OBABEL_MM"]

json_cache_main = [os.environ["DATA"] + "/00_datasets/DFT/cache_OD9_step7.json",
                   os.environ["DATA"] + "/00_datasets/DFT/cache_OPT_OD9_Marta_filtered.json",
                   os.environ["DATA"] + "/00_datasets/DFT/cache_OPT_OD9_0.json",
                   os.environ["DATA"] + "/00_datasets/DFT/cache_OPT.json",
                   os.environ["DATA"] + "/00_datasets/DFT/QM9/cache_QM9.json"]

json_cache_QM9 = [os.environ["DATA"] + "/00_datasets/DFT/QM9/cache_QM9.json"]
json_cache_OD9 = [os.environ["DATA"] + "/00_datasets/DFT/cache_OD9_step7.json"]

init_dataset_smiles = ["C"]

OD9_test_dataset_path = os.environ["DATA"] + "/00_datasets/DFT/OD9_7/OD9_7_smi_datasets/train_test_dataset_3000.smi"
QM9_test_dataset_path = os.environ["DATA"] + "/00_datasets/DFT/QM9/QM9_smi_datasets/train_test_dataset_100000.smi"

output_data_path = os.environ["DATA"] + "/07_BBO/03_bbo_optim/v4_no_cache_obabel/10.03_test_ulimit_njobs_optim_GAP_MBTR_RBF_batch_size_5_local"

# Parallel parameters
n_jobs_dft = 10
n_jobs_per_model = 5
batch_size = 5
pre_dispatch = 5


# BBO parameters
test_dataset_size = 3000
save_surrogate_model = False
period_save = 1
period_compute_test_predictions = 1
max_obj_calls = 20

# EvoMol parameters
evomol_pop_max_size = 300
evomol_max_steps = 10
evomol_k_to_replace = 10
evomol_init_pop_size = 10
evomol_n_runs = 10
evomol_n_best_retrieved = 1
evomol_init_pop_strategy = "random_weighted"

# Problem action space
max_heavy_atoms = 12
heavy_atoms = "C,N,O,F,S"

# GPR parameters (depends on the descriptor)
gpr_alpha = 1e-1
gpr_optimizer = 'fmin_l_bfgs_b'

# Merit parameters
EI_xi = 0.01
EI_noise_based = False
EI_init_pop_zero_EI = True

MM_program = "obabel"

# Gaussian Wrapper parameters
gaussian_wrapper_mu = 1.75
gaussian_wrapper_sigma = 1

cache_behaviour_main = "compute_again_delete_files"


# In[ ]:


from evomol.evomol.evaluation_dft import OPTEvaluationStrategy
from evomol.evomol.evaluation import GaussianWrapperEvaluationStrategy
from bbo.objective import EvoMolEvaluationStrategyWrapper

objective = EvoMolEvaluationStrategyWrapper(
    GaussianWrapperEvaluationStrategy(    
        evaluation_strategies=[OPTEvaluationStrategy(
                                    prop="gap",
                                    n_jobs=2,
                                    cache_files=json_cache_main,
                                    working_dir_path=dft_path,
                                    MM_program=MM_program,
                                    cache_behaviour=cache_behaviour_main
                               )],
        mu=gaussian_wrapper_mu,
        sigma=gaussian_wrapper_sigma
    ),
    n_jobs=n_jobs_dft
)

objective_QM9 = EvoMolEvaluationStrategyWrapper(
    GaussianWrapperEvaluationStrategy(    
        evaluation_strategies=[OPTEvaluationStrategy(
                                    prop="gap",
                                    n_jobs=2,
                                    cache_files=json_cache_QM9,
                                    working_dir_path=dft_path,
                                    MM_program=MM_program
                               )],
        mu=gaussian_wrapper_mu,
        sigma=gaussian_wrapper_sigma
    ),
    n_jobs=n_jobs_dft
)

objective_OD9 = EvoMolEvaluationStrategyWrapper(
    GaussianWrapperEvaluationStrategy(    
        evaluation_strategies=[OPTEvaluationStrategy(
                                    prop="gap",
                                    n_jobs=2,
                                    cache_files=json_cache_OD9,
                                    working_dir_path=dft_path,
                                    MM_program=MM_program
                               )],
        mu=gaussian_wrapper_mu,
        sigma=gaussian_wrapper_sigma
    ),
    n_jobs=n_jobs_dft
)


# In[ ]:


from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.gaussian_process.kernels import RBF, Matern, DotProduct

models = [("1*RBF(length_scale=1)", GaussianProcessRegressor(1.0*RBF(1.0), 
                                                             optimizer=gpr_optimizer, 
                                                             alpha=gpr_alpha))]




# In[ ]:


from bbo.model import GPRSurrogateModelWrapper

surrogate_wrappers = [
    ("GPRWrapper", GPRSurrogateModelWrapper),
]


# In[ ]:


from bbo.descriptor import MBTRDesc

descriptors = [
    ("MBTR_small", MBTRDesc(cache_location=None, cosine_angles_n=25, 
                            atomic_numbers_n=10, inverse_distances_n=25, 
                            species=["C", "H", "O", "N", "F", "S"], MM_program=MM_program,
                            n_jobs=n_jobs_per_model, batch_size=batch_size, pre_dispatch=pre_dispatch))
]


# In[ ]:


from bbo.merit import ExpectedImprovementMerit

merit_functions = [("Expected_improvement", ExpectedImprovementMerit, {"xi": EI_xi, 
                                                                       "init_pop_zero_EI": EI_init_pop_zero_EI,
                                                                       "noise_based": EI_noise_based})]


# In[ ]:





# In[ ]:



surrogate_models = []
    
for model in models:
    
    model_name, model_instance = model
    
    for surrogate_wrapper in surrogate_wrappers:

        wrapper_name, wrapper_class = surrogate_wrapper
        
        surrogate_models.append(
            (
                wrapper_name + "(" + model_name + ")", # Surrogate name
                wrapper_class(base_model=model_instance)
            )
        )
    
print(surrogate_models)


# In[ ]:





# In[ ]:


def build_grid(surrogate_models, descriptors, merit_functions):
    """
    Returning a list of tuple describing all compatible modules
    """
    
    grid = []
    
    for surrogate_model in surrogate_models:
        for descriptor in descriptors:
            for merit_function in merit_functions:
                grid.append((surrogate_model, descriptor, merit_function))
                        
    return grid
    


# In[ ]:


def load_smiles_from_file(path, n_smiles):
    
    smi_list = []
    
    with open(path, "r") as f:
        for line in f.readlines()[:n_smiles]:
            smi_list.append(line.rstrip())
            
    return smi_list
    


# In[ ]:


from os.path import join
from bbo.bboalg import BBOAlg
from bbo.stop_criterion import KObjFunCallsFunctionStopCriterion


def run_optimization_model(surrogate_model, descriptor, merit, run_id):

    surrogate_name = surrogate_model[0]
    surrogate_instance = surrogate_model[1]
    descriptor_name = descriptor[0]
    descriptor_instance = descriptor[1]
    merit_name = merit[0]
    Merit_class = merit[1]
    merit_parameters = merit[2]
    

    model_path = join(output_data_path, str(run_id))
    
    OD9_7_test_dataset_smiles = load_smiles_from_file(OD9_test_dataset_path, test_dataset_size)
    QM9_test_dataset_smiles = load_smiles_from_file(QM9_test_dataset_path, test_dataset_size)
    
    alg = BBOAlg(
        init_dataset_smiles=init_dataset_smiles,
        test_dataset_smiles_dict={
            "OD9_7": OD9_7_test_dataset_smiles, 
            "QM9": QM9_test_dataset_smiles
        },
        test_objectives_dict={
            "OD9_7": objective_OD9,
            "QM9": objective_QM9
        },
        descriptor=descriptor_instance,
        objective=objective,
        merit_function=Merit_class(descriptor=descriptor_instance, 
                                   surrogate=surrogate_instance,
                                   pipeline=None,
                                   **merit_parameters),
        surrogate=surrogate_instance,
        stop_criterion=KObjFunCallsFunctionStopCriterion(max_obj_calls),
        evomol_parameters={
            "optimization_parameters": {
                "pop_max_size": evomol_pop_max_size,
                "max_steps": evomol_max_steps,
                "k_to_replace": evomol_k_to_replace
            },
            "action_space_parameters":{
                "max_heavy_atoms": max_heavy_atoms,
                "atoms": heavy_atoms
            }
        },
        pipeline=None,
        evomol_init_pop_size=evomol_init_pop_size,
        n_evomol_runs=evomol_n_runs,
        n_best_evomol_retrieved=evomol_n_best_retrieved,
        evomol_init_pop_strategy=evomol_init_pop_strategy,
        results_path=model_path,
        n_jobs=n_jobs_per_model,
        batch_size=batch_size,
        pre_dispatch=pre_dispatch,
        save_surrogate_model=save_surrogate_model,
        period_save=period_save,
        period_compute_test_predictions=period_compute_test_predictions
        
    )
    
    alg.run()
    


# In[ ]:


import sys

if __name__ == "__main__":

    grid = build_grid(surrogate_models, descriptors, merit_functions)
    print(len(grid))
    
    input_param = int(sys.argv[1])
    
    model_id = input_param % len(grid)
    run_id = input_param // len(grid)
    
    surrogate_model, descriptor, merit = grid[model_id]
    
    run_optimization_model(surrogate_model, descriptor, merit, run_id=run_id)


# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:




