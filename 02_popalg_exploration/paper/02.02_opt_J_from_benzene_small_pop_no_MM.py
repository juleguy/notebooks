#!/usr/bin/env python
# coding: utf-8

# In[1]:


import os

output_folder_path = os.environ["DATA"] + "/02_popalg_exploration/paper/02_J_opt/02.02_J_opt_from_benzene_no_MM"

input_smiles_list = ["C1=CC=CC=C1"]

# Population algorithm
MAX_HEAVY_ATOMS = 38
MAX_POP_SIZE = 1000
TRASH_SIZE = 1000000
N_STEPS = 5000
N_TO_REPLACE = 10
N_MM_TRIES = 50
CHECK_MM = False
GRAPHOPS_DEPTH = 3
EXACTLY_K_ACTIONS = True
EPSILON_RANGE_STOP = 0.1
CONSECUTIVE_NO_REPLACEMENTS_STOP = 5

SELECTION = "KBEST"


# In[ ]:





# In[ ]:


from qupopalg.evaluation import PenalizedLogPEvaluationStrategy, LinearCombinationEvaluationStrategy, EntropyContribEvaluationStrategy
from graphops.actionspace import RemoveBondActionSpace, RemoveAtomActionSpace, SubstituteAtomActionSpace,     AddBondActionSpace, AddAtomActionSpace, ActionSpace
from qupopalg.insertion import KWorstInsertionStrategy, KUndefinedOrWorstInsertionStrategy
from qupopalg.mutation import KRandomGraphOpsImprovingMutationStrategy
from qupopalg.popalg import PopAlg
from qupopalg.selection import KBestSelectionStrategy, KRandomSelectionStrategy
from qupopalg.stopcriterion import KStepsStopCriterionStrategy, MultipleStopCriterionsStrategy, EpsilonRangeCriterionStrategy, ConsecutiveAllMutationsFailedCriterionStrategy
import numpy as np


if SELECTION == "KBEST":
    selection_strategy = KBestSelectionStrategy(N_TO_REPLACE)
elif SELECTION == "KRANDOM":
    selection_strategy = KRandomSelectionStrategy(N_TO_REPLACE)

evaluation_strategy = LinearCombinationEvaluationStrategy([PenalizedLogPEvaluationStrategy()],
                                                          [1])
accepted_atoms = ["C", "O", "N", "F"]
accepted_substitutions = {
    "C": ["O", "N", "F"],
    "O": ["C", "N", "F"],
    "N": ["C", "O", "F"],
    "F": ["C", "O", "N"]
}

parameters = ActionSpace.ActionSpaceParameters(max_heavy_atoms=MAX_HEAVY_ATOMS,
                                               accepted_atoms=accepted_atoms,
                                               accepted_substitutions=accepted_substitutions)

action_spaces = [
    AddAtomActionSpace(keep_connected=True),
    AddBondActionSpace(),
    SubstituteAtomActionSpace(),
    RemoveAtomActionSpace(keep_connected=True),
    RemoveBondActionSpace(keep_connected=True)]

pop_alg = PopAlg(
    selection_strategy=selection_strategy,
    insertion_strategy=KUndefinedOrWorstInsertionStrategy(MAX_POP_SIZE, N_TO_REPLACE),
    evaluation_strategy=evaluation_strategy,
    mutation_strategy=KRandomGraphOpsImprovingMutationStrategy(k=GRAPHOPS_DEPTH, max_n_try=N_MM_TRIES,
                                                               evaluation_strategy=evaluation_strategy,
                                                               action_spaces=action_spaces,
                                                               action_spaces_parameters=parameters,
                                                               check_MM=CHECK_MM,
                                                               exactly_k_actions=EXACTLY_K_ACTIONS),
    stop_criterion_strategy=MultipleStopCriterionsStrategy([KStepsStopCriterionStrategy(N_STEPS),
                                                            EpsilonRangeCriterionStrategy(EPSILON_RANGE_STOP),
                                                            ConsecutiveAllMutationsFailedCriterionStrategy(CONSECUTIVE_NO_REPLACEMENTS_STOP)]),
    pop_max_size=MAX_POP_SIZE,
    trash_size=TRASH_SIZE,
    output_folder_path=output_folder_path
)

pop_alg.stop_criterion_strategy.pop_alg = pop_alg

pop_alg.load_pop_from_smiles_list(smiles_list=input_smiles_list)


print()
print("Running the algorithm")
pop_alg.run()


# In[ ]:




