#!/usr/bin/env python
# coding: utf-8

# In[ ]:


import os

output_folder_path = os.environ["DATA"] + "/02_popalg_exploration/03_CLScore_opt/30.02_CLScore_opt_from_QMPC9_no_exploration"

input_smiles_path = os.environ["DATA"] + "/00_datasets/QMPC9_clean.csv"
SMILES_COL = 1

# Population algorithm
MAX_HEAVY_ATOMS = 30
N_MAX_DESC = 500000
MAX_POP_SIZE = 190154
TRASH_SIZE = 1000000
N_STEPS = 5000
N_TO_REPLACE = 100
N_MM_TRIES = 50
CHECK_MM = False
GRAPHOPS_DEPTH = 3


# In[ ]:





# In[ ]:


from qupopalg.evaluation import CLScoreEvaluationStrategy, LinearCombinationEvaluationStrategy, EntropyContribEvaluationStrategy
from graphops.actionspace import RemoveBondActionSpace, RemoveAtomActionSpace, SubstituteAtomActionSpace,     AddBondActionSpace, AddAtomActionSpace, ActionSpace
from qupopalg.insertion import KWorstInsertionStrategy, KUndefinedOrWorstInsertionStrategy
from qupopalg.mutation import KRandomGraphOpsImprovingMutationStrategy
from qupopalg.popalg import PopAlg
from qupopalg.selection import KBestSelectionStrategy
from qupopalg.stopcriterion import KStepsStopCriterionStrategy
import numpy as np



evaluation_strategy = LinearCombinationEvaluationStrategy([EntropyContribEvaluationStrategy(N_MAX_DESC, 
                                                                                            MAX_POP_SIZE, "ifg"),
                                                          EntropyContribEvaluationStrategy(N_MAX_DESC, 
                                                                                           MAX_POP_SIZE, "scf"),
                                                          CLScoreEvaluationStrategy()],
                                                          [0,0,1])

accepted_atoms = ["C", "O", "N", "F"]
accepted_substitutions = {
    "C": ["O", "N", "F"],
    "O": ["C", "N", "F"],
    "N": ["C", "O", "F"],
    "F": ["C", "O", "N"]
}

parameters = ActionSpace.ActionSpaceParameters(max_heavy_atoms=MAX_HEAVY_ATOMS,
                                               accepted_atoms=accepted_atoms,
                                               accepted_substitutions=accepted_substitutions)

action_spaces = [
    AddAtomActionSpace(keep_connected=True),
    AddBondActionSpace(),
    SubstituteAtomActionSpace(),
    RemoveAtomActionSpace(keep_connected=True),
    RemoveBondActionSpace(keep_connected=True)]

pop_alg = PopAlg(
    selection_strategy=KBestSelectionStrategy(N_TO_REPLACE),
    insertion_strategy=KUndefinedOrWorstInsertionStrategy(MAX_POP_SIZE, N_TO_REPLACE),
    evaluation_strategy=evaluation_strategy,
    mutation_strategy=KRandomGraphOpsImprovingMutationStrategy(k=GRAPHOPS_DEPTH, max_n_try=N_MM_TRIES,
                                                               evaluation_strategy=evaluation_strategy,
                                                               action_spaces=action_spaces,
                                                               action_spaces_parameters=parameters,
                                                               check_MM=CHECK_MM),
    stop_criterion_strategy=KStepsStopCriterionStrategy(N_STEPS),
    pop_max_size=MAX_POP_SIZE,
    trash_size=TRASH_SIZE,
    output_folder_path=output_folder_path
)

pop_alg.stop_criterion_strategy.pop_alg = pop_alg

pop_alg.load_pop_from_csv_file(input_smiles_path, col_idx=SMILES_COL, delimiter=",")


print()
print("Running the algorithm")
pop_alg.run()


# In[ ]:




