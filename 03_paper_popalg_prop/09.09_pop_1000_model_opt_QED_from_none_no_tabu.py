#!/usr/bin/env python
# coding: utf-8

# In[ ]:


import os

output_folder_path = os.environ["DATA"] + "/03_paper_popalg/01_QED_opt/09.09_pop_1000_QED_opt_from_none_no_tabu"

K_RUNS = 10

input_smiles_list = ["C"]

# Population algorithm
MAX_HEAVY_ATOMS = 40
MAX_POP_SIZE = 1000
N_TO_REPLACE = 10
N_MAX_DESC = 1000000
N_STEPS = 3000
FIND_IMPROVER_TRIES = 50
GRAPHOPS_DEPTH = 3

plateau = False

tabu_removed_ind = False


# In[ ]:





# In[1]:


from chempopalg.evaluation import PenalizedLogPEvaluationStrategy, LinearCombinationEvaluationStrategy, QEDEvaluationStrategy, CLScoreEvaluationStrategy, DescriptorCountEvaluation, NormalizedSAScoreEvaluationStrategy
from chempopalg.molgraphops.actionspace import RemoveBondActionSpace, RemoveAtomActionSpace, SubstituteAtomActionSpace,     AddBondActionSpace, AddAtomActionSpace, ActionSpace
from chempopalg.molgraphops.default_actionspaces import CONF_action_spaces
from chempopalg.mutation import KRandomGraphOpsImprovingMutationStrategy
from chempopalg.popalg import PopAlg
from chempopalg.stopcriterion import KStepsStopCriterionStrategy, KthScoreRelativeImprovementStopCriterionStrategy, MultipleStopCriterionsStrategy, KthScoreMaxValue, ConsecutiveAllMutationsFailedCriterionStrategy
import numpy as np
from os.path import join

def run(model_id):

    evaluation_strategy = LinearCombinationEvaluationStrategy([QEDEvaluationStrategy(),
                                                               PenalizedLogPEvaluationStrategy(),
                                                               CLScoreEvaluationStrategy(),
                                                               NormalizedSAScoreEvaluationStrategy(),
                                                               DescriptorCountEvaluation(N_MAX_DESC, 
                                                                                         MAX_POP_SIZE,
                                                                                         "efg"),
                                                               DescriptorCountEvaluation(N_MAX_DESC, 
                                                                                         MAX_POP_SIZE,
                                                                                         "scaf"),
                                                               DescriptorCountEvaluation(N_MAX_DESC, 
                                                                                         MAX_POP_SIZE,
                                                                                         "shg")
                                                              ],
                                                              [1, 0, 0, 0, 0, 0, 0])
    
    action_spaces, action_spaces_parameters = CONF_action_spaces(max_heavy_atoms=MAX_HEAVY_ATOMS)
    
    pop_alg = PopAlg(
        evaluation_strategy=evaluation_strategy,
        mutation_strategy=KRandomGraphOpsImprovingMutationStrategy(k=GRAPHOPS_DEPTH, max_n_try=FIND_IMPROVER_TRIES,
                                                                   evaluation_strategy=evaluation_strategy,
                                                                   action_spaces=action_spaces,
                                                                   action_spaces_parameters=action_spaces_parameters,
                                                                   plateau=plateau),
        stop_criterion_strategy=MultipleStopCriterionsStrategy([KStepsStopCriterionStrategy(N_STEPS)]),                                                                   
        pop_max_size=MAX_POP_SIZE,
        output_folder_path=join(output_folder_path, str(model_id)),
        k_to_replace=N_TO_REPLACE,
        tabu_removed_ind=tabu_removed_ind
    )

    pop_alg.stop_criterion_strategy.pop_alg = pop_alg

    pop_alg.load_pop_from_smiles_list(smiles_list=input_smiles_list)


    print()
    print("Running the algorithm " + str(model_id))
    pop_alg.run()
    


# In[ ]:


from joblib import Parallel, delayed

Parallel(n_jobs=K_RUNS)(
    delayed(run)(
        i) for i in range(K_RUNS)
);


# In[ ]:





# In[ ]:





# In[ ]:




