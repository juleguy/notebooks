#!/usr/bin/env python
# coding: utf-8

# In[1]:


import os
from os import makedirs

output_json_dir = os.environ["DATA"] + "/03_paper_popalg/05_guacamol/38_top_100_baseline_x10/"
makedirs(output_json_dir, exist_ok=True)

chembl_path = os.environ["DATA"] + "/00_datasets/Guacamol/guacamol_v1_best.smiles"

max_heavy_atoms = 50
graphops_depth = 3
find_improvers_tries = 50
stop_n_steps=1000
kth_ind_improvement_expected = 0.01
kth_ind_improvement_window = 300
max_consecutive_no_improver_found = 20
pop_max_size = 1000
max_mut_fail = float("inf")
tabu_removed_ind = True
cut_insert = False
select = "best"

init_top_100 = True

sanitize_mol = True

K_RUNS = 10


# In[4]:


from guacamol.assess_goal_directed_generation import assess_goal_directed_generation
from chempopalg.guacamol_binding import ChemPopAlgGoalDirectedGenerator
from os.path import join
from joblib import Parallel, delayed


def run(i):
    model_generator = ChemPopAlgGoalDirectedGenerator(chembl_path=chembl_path,
                                                      max_heavy_atoms=max_heavy_atoms,
                                                      graphops_depth=graphops_depth,
                                                      find_improvers_tries=find_improvers_tries,
                                                      stop_n_steps=stop_n_steps,
                                                      kth_ind_improvement_expected=kth_ind_improvement_expected,
                                                      kth_ind_improvement_window=kth_ind_improvement_window,
                                                      pop_max_size=pop_max_size,
                                                      max_mut_fail=max_mut_fail,
                                                      init_top_100=init_top_100,
                                                      tabu_removed_ind=tabu_removed_ind,
                                                      select=select,
                                                      models_dir=join(output_json_dir, str(i)),
                                                      max_consecutive_no_improver_found=max_consecutive_no_improver_found,
                                                      sanitize_mol=sanitize_mol)



    assess_goal_directed_generation(model_generator, json_output_file=join(output_json_dir,"output_v2" + str(i) + ".json"),
                                    benchmark_version='v2')

    assess_goal_directed_generation(model_generator, json_output_file=join(output_json_dir,"output_trivial" + str(i) + ".json"),
                                    benchmark_version='trivial')

import sys
    
if __name__ == "__main__":
    run(sys.argv[1])


# In[ ]:





# In[ ]:




